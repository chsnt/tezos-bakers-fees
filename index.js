/*
 * Данный скрипт является завершенным и самодостаточным,
 * он не является частью чего-то большего,
 * по этому стоит воспринимать его как готовый проект.
 * Соответственно ваш исправленный скрипт так же будет оцениваться
 * как готовое решение со всеми приложенными файлами.
 * В задачах ошибок нет и стоит быть внимательным при решении проблем.
 * Так же допускается пропуск одной из задач.
 *
 * Задачи:
 * 1) ✔️Исправить ошибки в скрипте
 * 2) ✔️Оптимизировать его приведя в "красивый" вид
 * 3) ➡Сократить потребление оперативной памяти
 * 4) ✔Сократить время исполнения скрипта
 * 5) ✔️Посчитать комиссию, которую получит каждый бейкер с блока 832543 по 832546 включительно
 */

const axios = require('axios');

const start = new Date().getTime();
const time = () => (new Date().getTime() - start) / 1000;
// Задача посчитать комиссию каждого бейкера - адрес конкретного нам не нужен
// const address = 'tz1TaLYBeGZD3yKVHQGBM857CcNnFFNceLYh';
const mainTezosBaseAddress = 'https://mainnet-tezos.giganode.io/chains/main/blocks/';
const firstBlockNumber = 832543;
const lastBlockNumber = 832546;

class TezosBlock {
  constructor(number) {
    this.number = number;
    this.data = null;
  }

  async loadData() { // Выгружаем данные из публичной ноды
    const url = new URL(String(this.number), mainTezosBaseAddress);
    const { data } = await axios.get(url.toString());
    this.data = data;
  }
}

const getBlocksList = async () => {
  const blocks = [];
  const requests = [];
  for (let block = firstBlockNumber; block <= lastBlockNumber; block++) {
    const Block = new TezosBlock(block);
    requests.push(Block.loadData());
    blocks.push(Block);
  }
  await Promise.all(requests);
  return blocks.map(block => block.data);
};

const getTransaction = list => list.map(batchs => batchs.operations).flat(2);

const calcBakerFees = transactions => {
  const bakersFees = {};

  transactions.forEach(tx => {
    if (!tx.contents) return;
    tx.contents.forEach(row => {
      if (!row.fee) return;
      if (!bakersFees[row.source]) bakersFees[row.source] = 0;
      bakersFees[row.source] += Number(row.fee);
    });
  });

  return bakersFees;
};

(async function () {
  // Загружаем список необходмых блоков
  const list = await getBlocksList();

  // Получаем транзакции
  const transactions = getTransaction(list);

  // Считаем комиссии для бейкеров
  const bakersFees = calcBakerFees(transactions);

  // Выводим результат
  console.log('Count transactions', transactions.length);
  console.log('Bakers fees', bakersFees);
  console.log('Memory (heapUsed, MB)', Math.round(((process.memoryUsage().heapUsed / 1024) / 1024) * 100) / 100);
  console.log('Time (seconds)', time());
}());
