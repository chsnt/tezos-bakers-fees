module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: 'airbnb-base',
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'arrow-parens': ['error', 'as-needed'],
  },
  overrides: [
    {
      files: ['*.js'],
      rules: {
        'no-plusplus': 'off',
      },
    },
  ],
};
